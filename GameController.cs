﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
    public static bool jugando;
    public Multiplier mp;
    public Score sc;
    public PlayerController pc;
    public Enemies en;
	// Use this for initialization
	void Start () {
        jugando = true;
	}
	

    public void iniciar()
    {
        jugando = true;
        pc.Reset();
        mp.Reset();
        sc.Reset();
        en.Reset();

    }

    public void fin()
    {
        jugando = false;
    }

    public bool estaJugando()
    {
        return jugando;
    }
}
