﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Multiplier : MonoBehaviour {
    public AudioSource fx;
    private int multiValue;
    Text multi;
	
	void Start () {
        multiValue = 1;
        multi = GetComponent<Text>();
        setText();
	}

    public void incMultiplier()
    {
        multiValue++;
        setText();
        fx.Play();

    }

    private void setText()
    {
        multi.text = "x" + multiValue;
    }

    internal void Reset()
    {
        Start();
    }

    public int getMulti()
    {
        return multiValue;
    }
}
