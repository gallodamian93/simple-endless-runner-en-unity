﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour {
    private float xspeed;
    private float nextXspeed;
    private float xseconds;
    public float factor_crecimiento;
    private float duracion;
    private bool xspeedChanged;
    public Multiplier mp;
    public GameController gc;
    private bool lastEnemie;
    private float spinValue;
 
    // Use this for initialization
    void Start () {
        xspeed = 0.2f;
        posicionar();
        transform.Translate(-0.1f * xspeed, 0, 0);
        xseconds = 0;
        xspeedChanged = false;
        lastEnemie = false;
        duracion = 5;

    }

    //transform.position += Vector3.up* speed;
    //transform.Rotate(rotSpeed,0,0);

    void spin()
    {
        transform.Rotate(0 , 0, spinValue);
    }

    void setSpin()
    {
        transform.eulerAngles=new Vector3(0, 0, 0);
        spinValue = UnityEngine.Random.Range(-1f, 1f);
    }


    internal void Reset()
    {
        Start();
    }

    void posicionar()
    {
        transform.position = new Vector2(12, UnityEngine.Random.Range(-4f, 3f));
    }


    // Update is called once per frame
    void Update () {
        xseconds += Time.deltaTime;
        spin();
        //limite
        if (transform.position.x <= -10 )
        {
            if (gc.estaJugando())
            {
                posicionar();
                setSpin();
                Score.scoreValue += mp.getMulti();
                if (xspeedChanged)
                {
                    xspeed = nextXspeed;
                    xspeedChanged = false;
                    mp.incMultiplier();
                }
            }
            else
            {
                transform.Translate(0, 0, 0);
                lastEnemie = true;
            }

            
        }
        if (lastEnemie == false) {
           // transform.Translate(-0.1f * xspeed, 0, 0);
            transform.position += Vector3.left* xspeed;
        }


        if (xseconds >= duracion && gc.estaJugando())
        {
            nextXspeed = xspeed +factor_crecimiento;
            xseconds = 0;
            xspeedChanged = true;
            duracion *= 1.5f;
        }
    }
}
