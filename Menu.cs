﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Menu : MonoBehaviour {

    public void Playgame()
    {
        SceneManager.LoadScene("minigame");
    }

    public void Principal()
    {
        SceneManager.LoadScene("Menu");
    }
}
