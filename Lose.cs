﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Lose : MonoBehaviour {

    Text finalScore;
    public static string strHighscore = "Highscore";
    int highscore;
	// Use this for initialization
	void Start () {
        finalScore = GetComponent<Text>();        
	}

    // Update is called once per frame
    void Update() {
        finalScore.text = "Score: " + Score.scoreValue;
        highscore = PlayerPrefs.GetInt(strHighscore);
        if (Score.scoreValue > highscore)
        {
            PlayerPrefs.SetInt(strHighscore, Score.scoreValue);
        }
    }

    public void mostrar()
    {
        gameObject.SetActive(true);
    }

    public void ocultar()
    {
        gameObject.SetActive(false);
    }
}



