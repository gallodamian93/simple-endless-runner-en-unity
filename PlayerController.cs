﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
    private Rigidbody rb;
    private float yspeed;
    public int angulo;
    private int maxAng =30;
    public GameController gc;
    public GameObject restart;
    public GameObject explosion;
    GameObject inst_explosion;
    public ParticleSystem turbina;
    public GameObject player;
    public AudioSource fx;




    void Start()
    {
        Destroy(GameObject.Find("cloneExp"));
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, 0, 0);
        rb.angularVelocity = new Vector3(0, 0, 0);
        transform.position = new Vector2(-5, 3);     
        yspeed = 0;
        inclinar();
        gameObject.SetActive(true);
        ActivaTurbina();

    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        
        yspeed = rb.velocity.y;
        if (Input.GetMouseButton(0))
        {
            Vector2 movement = new Vector2(0, 1);
            rb.AddForce(movement * 50);
        }
        inclinar();
        
      
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Obstacle")
        {
            fx.Play();
            gc.fin();
            restart.SetActive(true);
            Die();
            inst_explosion =Instantiate(explosion, transform.position, Quaternion.identity);
            inst_explosion.SetActive(true);
            inst_explosion.name = "cloneExp";

        }
    }

    void ActivaTurbina()
    {
        turbina.transform.SetParent(player.transform);
        turbina.transform.localPosition = new Vector2(-1.4f, 0.6f);
        turbina.transform.localEulerAngles = new Vector3(0, 270, 0);
        turbina.transform.localScale = new Vector3(1, 1, 1);
        turbina.Play();
    }

    void Die()
    {
        turbina.transform.parent = null;
        turbina.transform.localScale = new Vector3(1, 1, 1);
        turbina.Stop();
        gameObject.SetActive(false);
    }

    public void Reset()
    {
        Start();
    }

    void inclinar()
    {
        transform.eulerAngles = new Vector3(0, 0, yspeed * angulo);
    }

}
