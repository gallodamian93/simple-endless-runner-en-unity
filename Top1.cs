﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Top1 : MonoBehaviour {
    int top1;
    Text top;
	// Use this for initialization
	void Start () {
        top1 = PlayerPrefs.GetInt("Highscore");
        top = GetComponent<Text>();
        top.text = "Record: " + top1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
